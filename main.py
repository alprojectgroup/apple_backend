from typing import Union
from fastapi import FastAPI
from pydantic import BaseModel
import joblib
import sklearn
import numpy as np

print('The scikit-learn version is {}.'.fromat(sklearn._version_))
model = joblib.load('QuatityApplefinalmodel.pkl');
app = FastAPI()

class Item(BaseModel):
    Size : float
    Weight : float
    Sweetness : float
    Crunchiness : float
    Ripeness : float
    Acidity : float

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}

@app.put("/item")
async def update_item(item: Item):
    data = np.array([[item.Size, item.Weight, item.Sweetness, item.Crunchiness, item.Ripeness, item.Acidity]])
    answer = model.predict(data)
    print(answer[0])
    return {"Answer": answer[0]}